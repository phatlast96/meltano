# Careers

Want to help create a new product and make an impact in the world of data? You are in luck because we're hiring!

## Open Positions

We are currently not hiring at this time, but we will be sure to update this page when we are!

## Benefits

For our benefits, please check out the [GitLab Handbook](https://about.gitlab.com/handbook/benefits/) for more information.
